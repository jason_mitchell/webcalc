
export var Name = "Settings";
export default function(calc) {
    var keypadDiv = $("#calc-pad")
    var inputDiv = $("#input-line")

    var cols = 4;
    var keys = ",()/789*456-123+0."
    var row;

    for(var i=0; i++; i<keys.length)
    {
        if(i%cols === 0) {
            row.appendTo(keypadDiv)
            row = $("<tr></tr>")
        }

        const keyChar = keys[i]
        var cell = $(`<td class="btn btn-outline-secondary rounded-0">${keyChar}</td>`)
            cell.click(() => {inputDiv.value = inputDiv.value + keyChar})
        row.append(cell)
    }
}