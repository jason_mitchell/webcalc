import "./lib/js.storage.min.js";
"use strict";

export var Name = "calc";
export var Description = "RPN Calculator";
export default function(term)
{
    var store=Storages.initNamespaceStorage('webcalc').localStorage;
    
    const factorial = (a) => {
        var factorial = 1;
        for (var i = 2; i <= a; i++) {factorial *= i;}
        return factorial;
    }

    var calc = {}
    calc.functions = {};
    calc.variables = {};
    calc.lastAnswer = 0;
    calc.constants = { //not so constant constants
        "pi": () => Math.PI,
        "phi": () => (1+Math.sqrt(5))/2,
        "e": () => Math.E,
        "rand": () => Math.random(),
        "ans": () => calc.lastAnswer,
        "true": () => 1,
        "false": () => 0,
    }
    calc.operators = {
        "FACTORIAL" : {
            precedence:18,
            symbols: ["!"],
            arity: 1,
            left:false,
            evaluate: (a) => factorial(a)
        },
        "POWER" : { 
            precedence:17,
            symbols: ["^"],
            arity: 2,
            left:false,
            evaluate: (a,b) => b**a
        },
        "UNARY_ADDITION" : { 
            precedence:16,
            symbols: ["+"],
            arity: 1,
            left:false,
            evaluate: (a) => +a
        },
        "UNARY_SUBTRACTION" : {
            arity: 1,
            symbols: ["-"],
            precedence:16,
            left:false,
            evaluate: (a) => -a
        },
        "COMBINATION" : {
            arity: 2,
            symbols: ["ncr", "choose"],
            precedence:15,
            left:true,
            evaluate: (k,n) => Math.round(n<k ? 0 : factorial(n) / (factorial(k) * factorial(n-k)))
        },
        "PERUMTATION" : {
            arity: 2,
            symbols: ["npr"],
            precedence:15,
            left:true,
            evaluate: (k,n) => Math.round(factorial(n) / factorial(n-k))
        },
        "MODULUS" : {
            precedence:15,
            symbols: ["%"],
            arity: 2,
            left:true,
            evaluate: (a,b) => b % a
        },
        "MULTIPLICATION" : {
            precedence:14,
            symbols: ["*"],
            arity: 2,
            left:true,
            evaluate: (a,b) => a * b
        },
        "DIVISION" : {
            precedence:14,
            symbols: ["/"],
            arity: 2,
            left:true,
            evaluate: (a,b) => b / a
        },
        "ADDITION" : {
            precedence:13,
            symbols: ["+"],
            arity: 2,
            left:true,
            evaluate: (a,b) => a + b
        },
        "SUBTRACTION" : {
            precedence:13,
            symbols: ["-"],
            arity: 2,
            left:true,
            evaluate: (a,b) => b - a
        },
        "LESS_THAN" : {
            precedence:11,
            symbols: ["<"],
            arity: 2,
            left:true,
            evaluate: (a,b) => b < a ? 1 : 0
        },
        "LESS_THAN_OR_EQUAL" : {
            precedence:11,
            symbols: ["<="],
            arity: 2,
            left:true,
            evaluate: (a,b) => b <= a ? 1 : 0
        },
        "GREATER_THAN" : {
            precedence:11,
            symbols: [">"],
            arity: 2,
            left:true,
            evaluate: (a,b) => b > a ? 1 : 0
        },
        "GREATER_THAN_OR_EQUAL" : {
            arity: 2,
            symbols: [">="],
            precedence:11,
            left:true,
            evaluate: (a,b) => b >= a ? 1 : 0
        },
        "AND" : {
            precedence:6,
            symbols: ["&", "and"],
            arity: 2,
            left:true,
            evaluate: (a,b) => a && b ? 1 : 0
        },
        "OR" : {
            precedence:5,
            symbols: ["|", "or"],
            arity: 2,
            left:true,
            evaluate: (a,b) => a || b ? 1 : 0
        },
        "XOR" : {
            precedence:5,
            symbols: ["xor"],
            arity: 2,
            left:true,
            evaluate: (a,b) => ( a || b ) && !( a && b ) ? 1 : 0
        },
        "ASSIGNMENT_EQUALS" : {
            arity: 2,
            symbols: ["="],
            assignment: true,
            precedence: 3,
            left:false,
            evaluate: function(n,v) {
                //make sure the potential var is only letters, and doesnt match a constant or function
                if(/^[a-z]+$/.test(v) && calc.constants[v]===undefined && calc.functions[v]===undefined) {
                    calc.variables[v] = n;
                    console.log(calc.variables);
                    return n;
                }
                else
                {
                    throw SyntaxError("Variable names must be letters only, and not match a constant or function")
                }
            }
        },
    }
    
    /**
     * Loads an external function library
     * @param {string} Path to library
     */
    calc.loadLibrary = async function(path, ...args)
    {
        var lib = await import(path)
        if(lib.default(this)!==undefined)
        {
            calc.functions = Object.assign(calc.functions, lib.default(this))
            
            $("#functionMenu").append(
                $(`<li class="list-group-item list-group-item-action list-group-item-command p-1">${lib.Name}</li>`).click(() => {calc.showFunction(lib.Name.toLowerCase())})
            );
        }
    }
    
    calc.loadLibraries = async function(libraries, ...args)
    {
        console.log("Loading Libraries", libraries);
        for(var i=0; i < libraries.length; i++)
        {
            var lib = await calc.loadLibrary(libraries[i], ...args);
            console.log("Loaded Library", lib);
        }
        return true;
    }


    calc.showFunction = function(category)
    {
        var table = [];
        term.newCard()

        for (var key in calc.functions) {
            if (!calc.functions.hasOwnProperty(key)) continue;
            
            var fn = calc.functions[key];
            if(fn.category===category) 
            {
                table.push([fn.syntax,fn.description])
            }
        }
        term.table("info",table,["Syntax","Description"])
        term.endCard()
    }

    class Token {
        constructor(type, value) {
            this.type = type;
            this.value = value;
        }
    }

    function top(a) {
        if(a.length)
        {
            return a[a.length-1]
        }
        else
        {
            return new Token("","");
        }
    };

    function getSymbols()
    {
        let symbols = [];
        Object.values(calc.operators).forEach((a) =>
        {
            a.symbols.forEach((b) =>
            {
                symbols.push(b);
            });
        });
        return symbols;
    }

    calc.tokenize = function(str)
    {
        var tokens = []

        str = str.toLowerCase().replace(/\s+/g, "");
        const symbols = getSymbols();
        const sortedOps = symbols.sort((a,b) => b.length - a.length);
        const sortedFns = Object.keys(calc.functions).sort((a,b) => b.length - a.length);
        
        function isNumeric(s) {return /^[0-9.]+$/.test(s);} //matches 0-9 and decimal
        function isLetter(s) {return /[a-z]/i.test(s);} //matches a-z

        /**
         * Create an instance of a token of token type and value
         * @param {string} type 
         * @param {string} value 
         */
        function pushToken(type, value) { tokens.push(new Token(type,value)); }

        var tokenBuilder = {
            type: "",
            buffer: [],
            push: (c) => {tokenBuilder.buffer.push(c)},
            flush: () => {
                if(tokenBuilder.buffer.length)
                {
                    let token = pushToken(tokenBuilder.type, tokenBuilder.buffer.join("")); 
                    tokenBuilder.buffer = [];
                    tokenBuilder.type = "";
                    return token;
                }
            }
        }
        
        for(var i=0; i < str.length; i++)
        {
            const char = str[i];
            //Find the type of the current character
            let t = "None";

            if(isNumeric(char)) { t = "Literal"; }
            else if(isLetter(char)) { t = "Variable"; }
            else if(char == ",") {  t = "Separator"; }
            else if(char == "(") { t = "Left Parenthesis"; }
            else if(char == ")") { t = "Right Parenthesis"; }
            else if(char == '"') { t = "String"; }

            //See if type has changed
            if(t != tokenBuilder.type || char==="(" || char===")" || char===",")
            {
                tokenBuilder.flush();

                let func = sortedFns.some((fn) => {
                    let substr = str.substring(i, i + fn.length);
    
                    if(fn === substr)
                    {
                        pushToken("Function", fn);
                        
                        i += fn.length-1;
                        return true;
                    }
                });

                if(func)
                {
                    continue;
                }
                else
                {
                    let op = sortedOps.some((op) => {
                        let substr = str.substring(i, i+op.length);
    
                        if(op === substr)
                        {
                            pushToken("Operator", op);
                            
                            i += op.length-1;
                            return true;
                        }
                    });

                    if(op)
                    {
                        continue;
                    }
                    else
                    {
                        tokenBuilder.type = t;
                    }
                }
            }
            tokenBuilder.push(char);
        }
                
        tokenBuilder.flush();
        
        if(calc.dev.log===true)
        {
            var debug = tokens.map((t)=>t.value).join(" ");
            term.line("log", `tokens: ${debug}`)
        }
        console.log("tokens: ",tokens);

        return tokens;
    }
    
    /**
     * Prepare a list of tokens for the sya function, fixing any mathy stuff
     * @param {array} tokens
     */
    calc.prepare = function(tokens)
    {
        var infix = [];
        var lastType = "";
        var lastArity = undefined;

        function pushMultiplication() {
            //push a binary multiplier
            let t = new Token("Operator","*")
            t.arity = 2;
            t.operator = calc.operators["MULTIPLICATION"];
            
            infix.push(t)
        }

        tokens.forEach((token) =>
        {
            var thisType = token.type;
            
            if(thisType==="Literal")
            {
                if(lastType==="Variable") { pushMultiplication() }
            }
            else if(thisType==="Variable")
            {
                if(lastType==="Literal") { pushMultiplication() }
            }
            else if(thisType==="Function")
            {
                if(lastType==="Literal" || lastType==="Right Parenthesis") { pushMultiplication() }
            }
            else if(thisType==="Operator")
            {
                //Find the operators that match this token and sort them by arity
                let operators = Object.values(calc.operators).filter(function(obj)
                {
                    return obj.symbols.includes(token.value)
                }).sort((a,b) => (b.arity-a.arity));
                
                //Find the best matching operator, starting with highest arity
                let operator = operators.find(op =>
                {
                    if(op.arity === 2 && (lastType==="Literal" || lastType==="Variable" || lastType==="Right Parenthesis" || (lastType === "Operator" && lastArity === 1)))
                    {
                        return true;
                    }
                    else if(op.arity === 1 && (lastType==="" || lastType==="Separator" || lastType==="Literal" || lastType==="Left Parenthesis" || lastType==="Operator"))
                    {
                        return true;
                    }
                });

                if(operator)
                {
                    token.arity = operator.arity;
                    token.operator = operator;
                }
            }
            else if(thisType==="Left Parenthesis")
            {
                if(lastType==="Right Parenthesis" || lastType==="Variable" || lastType==="Literal")
                { 
                    pushMultiplication()
                }
            }

            infix.push(token);
            lastType = token.type;
            lastArity = token.arity;
        });
        
        if(calc.dev.log===true)
        {
            var debug = infix.map((t)=>t.value).join(" ");
            term.line("log", `infix: ${debug}`)
        }
        console.log("infix: ",tokens);
        return infix;
    }

    /**
     * Shunting Yard Algorithm.
     * Takes an infixed array of tokens and arranges it into postfix array.
     * @param {array} Infix tokens
     */
    calc.sya = function(tokens)
    {
        var queue = [];
        var stack = [];
        var debug = [];

        function runDebug(action) 
        {
            if(calc.dev.debug===true)
            {
                let currentStack = stack.map((t)=>t.value).join(" ")
                let currentQueue = queue.map((t)=>t.value).join(" ")
                
                debug.push([i, token.value, currentStack, currentQueue])
            }
        }

        function pushQueue(t) { queue.push(t); runDebug(`Pushed ${t.value} to queue`); }

        function pushStack(t) { stack.push(t); runDebug(`Pushed ${t.value} to stack`); }
        
        function popStack() { 
            let t = stack.pop();
            runDebug(`Popped ${t.value} off stack`);
            return t;
        }
        
        function popStackToQueue() {
            let t = stack.pop();
            queue.push(t);
            runDebug(`Popped ${t.value} off stack onto queue`);
        }

        for(var i=0; i<tokens.length; i++)
        {
            var token = tokens[i];
            var type = token.type;

            // If the token is a number (Literal), then add it to the output queue.
            if(type === "Literal")
            {
                pushQueue(token)
            }
            // If the token is a variable (Literal), add it to the output queue.
            else if(type === "Variable")
            {
                if(calc.constants[token.value])
                {
                    pushQueue(new Token("Literal", calc.constants[token.value]()));
                }
                else if(calc.variables[token.value])
                {
                    let next = tokens[i+1];
                    if(next !== undefined && next.operator.assignment) //Next operator is an assignment op, so push the token as is
                    {
                        pushQueue(token);
                    }
                    else //Read the variable and push it as a literal
                    {
                        pushQueue(new Token("Literal", calc.variables[token.value]));
                    }
                }
                else
                {
                    pushQueue(token);
                }
            }
            // If the token is a function token, then push it onto the stack.
            else if(type === "Function")
            {
                pushStack(token);
            }
            // If the token is a function argument separator (e.g., a comma):
            else if(type === "Separator")
            {
                // Until the token at the top of the stack is a left parenthesis,
                // pop operators off the stack onto the output queue.
                while(stack.length && top(stack).type !== "Left Parenthesis")
                {
                    popStackToQueue();
                }
                if(stack.length===0) throw new SyntaxError("Separator or parenthesis mismatched");
            }
            // If the token is an operator, op1, then:
            else if(type === "Operator")
            {
                // While there is an operator token, o2, at the top of the stack
                // op1 is left-associative and its precedence is less than or equal to that of op2,
                // or op1 is right-associative and its precedence is less than that of op2,
                while(stack.length && top(stack).operator !== undefined && token.operator)
                {
                    const topToken = top(stack)
                    if(topToken.operator)
                    {
                        const op1 = token.operator;
                        const op2 = topToken.operator;
                        if(op2.precedence > op1.precedence)
                        {
                            popStackToQueue()
                            continue;
                        }

                        if(op2.precedence === op1.precedence && op2.left)
                        {
                            popStackToQueue()
                            continue;
                        }
                    }

                    if(stack.length===0) throw new SyntaxError("Mismatch in parenthesis!");

                    break;
                }

                pushStack(token);
            }
            // If the token is a left parenthesis, then push it onto the stack.
            else if(type === "Left Parenthesis")
            {
                const topFn = calc.functions[top(stack).value]
                if(topFn !== undefined)
                {
                    pushQueue(new Token("Function Start",":"))
                }
                pushStack(token);
            }
            // If the token is a right parenthesis:
            else if(type === "Right Parenthesis")
            {
                // Until the token at the top of the stack is a left parenthesis,
                // pop operators off the stack onto the output queue
                while(stack.length)
                {
                    if(top(stack).type === "Left Parenthesis")
                    {
                        // Pop the left parenthesis from the stack, but not onto the output queue.
                        popStack()
                        break;
                    }
                    popStackToQueue();
                }
                // If the token at the top of the stack is a function token, pop it onto the output queue.
                if(top(stack).type === "Function") 
                {
                    popStackToQueue();
                }
            }
            else
            {
                throw "Unknown token "+token.value;
            }
        };
        
        // When there are no more tokens to read:
        while(stack.length)
        {
            // While there are still operator tokens in the stack:
            let token = popStack()
            if (token.type === "Left Parenthesis" || token.type === "Right Parenthesis") 
            {
                throw new SyntaxError("Parentheses mismatched");
            }
            pushQueue(token)
        }

        console.log("postfix: ",queue)

        if(calc.dev.log===true)
        {
            var debug = queue.map((t)=>t.value).join(" ");
            term.line("log", `postfix: ${debug}`)
        }
        if(calc.dev.debug===true && debug.length>0)
        {
            term.table("debug", debug, ["#","Token","Operator Stack","Postfix Queue",])
        }
        return queue;
    }

    /**
     * Reverse Polish Notation.
     * Uses RPN to evaluate a postfix expression.
     * @param {array} Postfix tokens 
     */
    calc.rpn = function(tokens)
    {
        var stack = [];
        var debug = [];

        function runDebug(action)
        {
            if(calc.dev.debug===true)
            {
                let currentStack = stack.map((t) => t.value).join(" ")
                let currentQueue = tokens.filter((t,j) => j>i).map((t) => t.value).join(" ")

                debug.push([i, token.value, currentStack, currentQueue])
                //term.line("debug", `${i+1} rpn(${token.value}): Stack[${currentStack}] Queue[${leftoverTokens}]`)
            }       
        }

        function pushStack(t) {stack.push(t); runDebug(`Pushed ${t.value} to stack`);}
        function popStack() {
            let t = stack.pop()
            runDebug(`Popped ${t.value} from stack`);
            return t
        }

        for(var i=0; i<tokens.length; i++)
        {
            var token = tokens[i];

            if(token.type === "Literal")
            {
                pushStack(token)
            }
            else if(token.type === "Variable")
            {
                pushStack(token)
            }
            else if(token.type === "Function Start")
            {
                pushStack(token)
            }
            else if(token.type === "Operator")
            {
                const op = token.operator;

                var args = [];
                var arity = 0;
                while(arity < op.arity && stack.length)
                {
                    arity++
                    var t = popStack()
                    if(t.type==="Literal")
                    {
                        args.push(parseFloat(t.value))
                    }
                    else if(t.type==="Variable")
                    {
                        args.push(t.value);
                    }
                }

                pushStack(new Token("Literal", op.evaluate(...args)));
            }
            else if(token.type === "Function")
            {
                const fn = calc.functions[token.value];
                
                var args = [];
                while(stack.length)
                {
                    var t = popStack()
                    if(t.type==="Literal")
                    {
                        args.push(parseFloat(t.value))
                    }
                    else if(t.type==="Variable")
                    {
                        args.push(t.value)
                    }
                    else if(t.type==="Function Start")
                    {
                        break;
                    }
                }
                pushStack(new Token("Literal", fn.evaluate(...args)));
            }
        }

        if (stack.length>1) {term.line("warning", "Leftover tokens!");}

        if(calc.dev.debug===true && debug.length>0)
        {
            term.table("debug", debug, ["#","Token","Result Stack","Postfix Queue",])
        }

        const result = stack.pop().value;
        calc.lastAnswer = result;
        return result;
    }

    calc.format = function(result)
    {
        switch(calc.number.mode)
        {
            case "decimal":
                if(calc.number.comma === true) { result = result.replace(/\B(?=(\d{3})+(?!\d))/g, ","); }
                return result;
            case "scientific":
                return result.toExponential();
            case "fractional":
                return (new Fraction(result)).toFraction(true);
        }
    }

    /**
     * Evaluates an Input expression string
     * @param {string} Input expression string 
     */
    calc.eval = function(string)
    {
        return calc.rpn(calc.sya(calc.prepare(calc.tokenize(string))));
    }
    return calc;
}
