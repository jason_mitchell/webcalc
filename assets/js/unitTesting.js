export function TestUnits(term, calc)
{
    let passed = 0;
    let failed = 0;
    function testExpression(expression, expected)
    {
        try
        {
            let card = term.newCard("log", "expression-test");
            term.line("log", `Testing expression "${expression}"`);
            
            let result = calc.eval(expression);
            term.line("log", `Result: ${result}`);
            term.line("log", `Expected: ${expected}`);
            
            if(result != expected)
            {
                term.line("error", `Expression Test "${expression}" Failed. Result: ${result} Expected: ${expected}`);
                failed++;
                term.endCard();
                return false;
            }
            else
            {
                passed++;
                term.endCard();
                card.remove();
                return true;
            }
        }
        catch(e)
        {
            failed++;
            console.error(e);
            term.line("error", e);
        }
        return false;
    }

    //order of operations
    testExpression("3+4*2/(1-5)^2^3", 3.0001220703125) //wikipedia example

    testExpression("-6*4(-1)", 24)
    testExpression("(-6/6)^3", -1)
    testExpression("3+(8)/abs(4)", 5)
    testExpression("5(-5+6)*6^2", 180)
    testExpression("8/4*2", 4)
    testExpression("7-5+6", 8)
    testExpression("(-9-(2-5))/(-6)", 1)
    testExpression("(-2*2^3*2)/(-4)", 8)
    testExpression("-6+(-3-3)^2/abs(3)", 6)
    testExpression("(-7-5)/(-2-2-(-6))", -6)

    testExpression("4-2abs(3^2-16)", -10)
    testExpression("((-10-6)/(-2^2))-5", -1)
    testExpression("(-1-(-5))abs(3+2)", 20)
    testExpression("-3-(3-(-3(2+4)-(-2)))", -22)
    testExpression("(2+4abs(7+2^2))/(4*2+5*3)", 2)
    testExpression("-4-(2+4(-6)-4-abs(2^2-5*2))", 28)
    testExpression("(6*2+2-(-6))(-5+abs(-18/6))", -40)
    testExpression("2*(-3)+3-6(-2-(-1-3))", -15)
    testExpression("(-13-2) / (2-(-1)^3+(-6)-(-1-(-3)))", 3)
    testExpression("(-5^2 + (-5)^2) / (abs(4^2 - 2^5) - 2*3)", 0)

    testExpression("6 * ((-8-4+(-4)-(-4-(-3))) / ((4^2 + 3^2) / 5))", -18)
    testExpression("(-9*2-(3-6))/(1-(-2+1)-(-3))", -3)
    testExpression("(2^3+4)/(-18-6+(-4)-(-5(-1)(-5)))", -4)
    testExpression("(13 + (-3)^2 + 4(-3) + 1 - (-10 - (-6))) / (((4+5) / (4^2 - 3^2(4-3) - 8)) + 12)", 5)
    testExpression("(5+3^2-24/6*2)/((5+3(2^2-5))+abs(2^2-5)^2)", 2)

    //Logic Tests
    testExpression("0 and 1 or 1", 1)
    testExpression("0 and (1 or 1)", 0)
    testExpression("4 and -4 xor (.6 and 0)", 1)
    testExpression("not(1) xor (1 and 1 xor 1)", 0)
    testExpression("1 and 0 xor (6*4 and 0) or not(0 and 6)", 1)
    testExpression("1 and (1 xor not(5 xor 1 and 0)) xor not(1 xor not(1 or not(1)))", 1)

    testExpression("4! + 5!", 144)

    term.newCard();
    term.line("success", `${passed} test${passed == 1?"":"s"} successfully completed.`);
    if(failed > 0)
    {
        term.line("error", `${failed} test${failed == 1?"":"s"} failed.`);
    }
    term.endCard();

    return failed == 0;
}