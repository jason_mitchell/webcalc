
export var Name = "Statistics"
export default function(calc) {
    function max(...array) {
        return Math.max(...array);
    }

    function min(...array) {
        return Math.min(...array);
    }

    function range(...array) {
        return max(...array) - min(...array);
    }

    function midrange(...array) {
        return range(...array) / 2;
    }

    function sum(...array) {
        var num = 0;
        for (var i = 0, l = array.length; i < l; i++) num += array[i];
        return num;
    }

    function sumSquares(...array) {
        var num = 0;
        for (var i = 0, l = array.length; i < l; i++) num += array[i]**2;
        return num;
    }

    function prod(...array) {
        var num = 1;
        for (var i = 0, l = array.length; i < l; i++) num *= array[i];
        return num;
    }

    function mean(...array) {
        return sum(...array) / array.length;
    }

    function median(...array) {
        array.sort(function(a, b) { return a - b; });
        var mid = array.length / 2;
        return mid % 1 ? array[mid - 0.5] : (array[mid - 1] + array[mid]) / 2;
    }

    function variancePop(...array) {
        let avg = mean(...array)
        return mean(...array.map((num) => (avg - num)**2))
    }

    function varianceSam(...array) {
        return ((Math.pow(sum(...array),2) / array.length) - sumSquares(...array)) / (array.length-1)
    }

    function standardDevaitionPop(...array) {
        return Math.sqrt(variancePop(...array));
    }
    
    function standardDeviationSam(...array) {
        return Math.sqrt(varianceSam(...array));
    }

    return {
        "count": {
            category: "statistics",
            syntax: "count(value1, [value2...])",
            description: "Returns a count of the number of values in a dataset.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return args.length;
                }
            },
        },
        "min": {
            category: "statistics",
            syntax: "min(value1, [value2...])",
            description: "Returns the minimum value in a numeric dataset.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return min(...args)
                }
            },
        },
        "max": {
            category: "statistics",
            syntax: "max(value1, [value2...])",
            description: "Returns the maximum value in a numeric dataset.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return max(...args)
                }
            },
        },
        "sum": {
            category: "statistics",
            syntax: "sum(value1, [value2...])",
            description: "Returns the sum of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return sum(...args)
                }
            },
        },
        "sumsq": {
            category: "statistics",
            syntax: "sumsq(value1, [value2...])",
            description: "Returns the sum of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return sumSquares(...args)
                }
            },
        },
        "prod": {
            category: "statistics",
            syntax: "sum(value1, [value2...])",
            description: "Returns the sum of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return prod(...args)
                }
            },
        },
        "mean": {
            category: "statistics",
            syntax: "mean(value1, [value2...])",
            description: "Returns the mean/average of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return mean(...args)
                }
            },
        },
        "avg": {
            category: "statistics",
            syntax: "average(value1, [value2...])",
            description: "Returns the mean/average of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return mean(...args)
                }
            },
        },
        "median": {
            category: "statistics",
            syntax: "mean(value1, [value2...])",
            description: "Returns the mean/average of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return median(...args)
                }
            },
        },
        "range": {
            category: "statistics",
            syntax: "range(value1, [value2...])",
            description: "Returns the range of a set of numbers.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return range(...args)
                }
            },
        },
        "var": {
            category: "statistics",
            syntax: "var(value1, [value2...])",
            description: "Calculates the variance based on a sample.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return varianceSam(...args)
                }
            },
        },
        "varp": {
            category: "statistics",
            syntax: "varp(value1, [value2...])",
            description: "Calculates the variance based on an entire population",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return variancePop(...args)
                }
            },
        },
        "stdev": {
            category: "statistics",
            syntax: "stdev(value1, [value2...])",
            description: "Calculates the standard deviation based on a sample.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return standardDeviationSam(...args)
                }
            },
        },
        "stdevp": {
            category: "statistics",
            syntax: "stdevp(value1, [value2...])",
            description: "Calculates the standard deviation based on an entire population.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 0: throw new SyntaxError("Invalid number of arguments")
                    default: return standardDeviationPop(...args)
                }
            },
        },
    }
}