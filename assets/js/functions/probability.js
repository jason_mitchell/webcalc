export function factorial(num)
{
    var factorial = 1;
    for (var i = 2; i <= num; i++) {factorial *= i;}
    return factorial;
}
export var Name = "Probability"
export default function(calc) {
    return {
        "factorial": {
            category: "probability",
            syntax: "factorial(integer)",
            description: "Returns the factorial of a integer.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return factorial(arguments[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "random": {
            syntax: "random(number, number)",
            category: "probability",
            description: "Returns a random integer between the specified numbers.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return (Math.random() * (args[1] - args[0])) + args[0];
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "randint": {
            syntax: "randint(integer, integer)",
            category: "probability",
            description: "Returns a random integer between the specified integers.",
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return Math.floor(Math.random() * (args[1] - args[0] + 1)) + args[0];;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
    }
}