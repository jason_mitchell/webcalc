export var Name = "Trigonometry";
export default function(calc) {
    /*===================
        Basic Trig
    ===================*/
    return {
        "sin":{
            category: "trigonometry",
            syntax: "sin(angle)",
            description: "Returns the sine of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.sin(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "cos":{
            category: "trigonometry",
            syntax: "cos(angle)",
            description: "Returns the cosine of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.cos(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "tan":{
            category: "trigonometry",
            syntax: "tan(angle)",
            description: "Returns the tangent of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.tan(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        // Reciprocols
        "csc":{
            category: "trigonometry",
            syntax: "csc(angle)",
            description: "Returns the cosecant of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.sin(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "sec":{
            category: "trigonometry",
            syntax: "sec(angle)",
            description: "Returns the secant of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.cos(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "cot":{
            category: "trigonometry",
            syntax: "cot(angle)",
            description: "Returns the cotangent of a radian or degree.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.tan(args[0] * calc.angle.factor);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "hypot":{
            category: "trigonometry",
            syntax: "hypot(number, number)",
            description: "Returns the square root of the sum of squares.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return Math.hypot(args[0],args[1]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        /*=====================
            Inverse Trig
        =====================*/
        "asin":{
            category: "trigonometry",
            syntax: "asin(number)",
            description: "Returns the inverse sine of a number in degrees or radians.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.asin(args[0]) / calc.angle.factor;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "acos":{
            category: "trigonometry",
            syntax: "acos(number)",
            description: "Returns the inverse cosine of a number in degrees or radians.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.acos(args[0]) / calc.angle.factor;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "atan":{
            category: "trigonometry",
            syntax: "atan(number)",
            description: "Returns the inverse tangent of a number in degrees or radians.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.atan(args[0]) / calc.angle.factor;
                    case 2: return Math.atan2(args[0],args[1]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        /*===================
            Hyperbolics
        ===================*/
        "sinh":{
            category: "trigonometry",
            syntax: "sinh(number)",
            description: "Returns the hyperbolic sine of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.sinh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "cosh":{
            category: "trigonometry",
            syntax: "cosh(number)",
            description: "Returns the hyperbolic cosine of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.cosh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "tanh":{
            category: "trigonometry",
            syntax: "tanh(number)",
            description: "Returns the hyperbolic tangent of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.tanh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "csch":{
            category: "trigonometry",
            syntax: "csch(number)",
            description: "Returns the hyperbolic cosecant of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.sinh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "sech":{
            category: "trigonometry",
            syntax: "sech(number)",
            description: "Returns the hyperbolic sine of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.cosh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "coth":{
            category: "trigonometry",
            syntax: "coth(number)",
            description: "Returns the hyperbolic cotangent of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return 1 / Math.tanh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        /*=========================
            Inverse Hyperbolics
        =========================*/
        "asinh":{
            category: "trigonometry",
            syntax: "asinh(number)",
            description: "Returns the inverse hyperbolic sine of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.asinh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "acosh":{
            category: "trigonometry",
            syntax: "acosh(number)",
            description: "Returns the inverse hyperbolic cosine of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.acosh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "atanh":{
            category: "trigonometry",
            syntax: "atanh(number)",
            description: "Returns the inverse hyperbolic tangent of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.atanh(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
    }
}