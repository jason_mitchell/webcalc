export var Name = "Logic"
export default function(calc,term) {
    return {
        "not":{
            category: "logic",
            syntax: "not(number)",
            description: "Returns the logical inverse of a statement or condition.",
            arity:1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return args[0]===0 ? 1 : 0;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "iseven":{
            category: "logic",
            syntax: "iseven(number)",
            description: "Checks whether the provided value is even.",
            arity:1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return args[0]%2==0 ? 1 : 0;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "isodd":{
            category: "logic",
            syntax: "isodd(number)",
            description: "Checks whether the provided value is odd.",
            arity:1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return args[0]%2==0 ? 0 : 1;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
    }
}