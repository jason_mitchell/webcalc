function gcd_two_numbers(x, y) {
    x = Math.abs(x);
    y = Math.abs(y);
    while(y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}

function lcm_two_numbers(x, y) {
    if ((typeof x !== 'number') || (typeof y !== 'number')) return false;
    return (!x || !y) ? 0 : Math.abs((x * y) / gcd_two_numbers(x, y));
}

const isPrime = num => {
    for(let i = 2; i < num; i++)
        if(num % i === 0) return false;
    return num !== 1;
}

export var Name = "Numbers";
export default function(calc) {
    return {
        "abs": {
            category: "numbers",
            syntax: "abs(value)",
            description: "Returns the absolute value of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.abs(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "sign": {
            category: "numbers",
            syntax: "sign(number)",
            description: "Return the sign of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.sign(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "ipart": {
            category: "numbers",
            syntax: "ipart(number)",
            description: "Return the integer part of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.trunc(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "fpart": {
            category: "numbers",
            syntax: "fpart(number)",
            description: "Return the float(decimal) part of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return args[0] % 1;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "int": {
            category: "numbers",
            syntax: "int(number)",
            description: "Rounds a number down to the nearest integer that is less than or equal to it.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.floor(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "floor": {
            category: "numbers",
            syntax: "floor(number)",
            description: "The FLOOR function rounds a number down to the nearest integer multiple of specified significance.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.floor(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "ceil": {
            category: "numbers",
            syntax: "ceil(number)",
            description: "The CEILING function rounds a number up to the nearest integer multiple of specified significance.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.ceil(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "round": {
            category: "numbers",
            syntax: "round(value, [precision])",
            description: "Rounds the number to the nearest whole number.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return args[0].toFixed();
                    case 2: return args[1].toFixed(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        /*============================
            Powers, Roots, Logs
        ============================*/
        "exp": {
            category: "numbers",
            syntax: "exp(number)",
            description: "Returns Euler's number, e (~2.718) raised to a power.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.exp(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "pow": {
            category: "numbers",
            syntax: "exp(number)",
            description: "Returns Euler's number, e (~2.718) raised to a power.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return Math.pow(args[0], args[1]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "sqrt": {
            category: "numbers",
            syntax: "sqrt(number)",
            description: "Returns the square root of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.sqrt(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "cbrt": {
            category: "numbers",
            syntax: "cbrt(number)",
            description: "Returns the cube root of a number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.cbrt(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "root": {
            category: "numbers",
            syntax: "root(number, base)",
            description: "Returns the specified root of a number.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return Math.pow(args[1], 1/args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "ln": {
            category: "numbers",
            syntax: "ln(number)",
            description: "Returns the the logarithm of a number, base e (Euler's number).",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.log(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "log": {
            category: "numbers",
            syntax: "log(number, [base])",
            description: "Returns the the logarithm of a number given a base.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.log10(args[0]);
                    case 2: return Math.log(args[1]) / Math.log(args[0])
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "log10": {
            category: "numbers",
            syntax: "log10(number)",
            description: "Returns the the logarithm of a number, base 10.",
            arity: Infinity,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return Math.log10(args[0]);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        /*============================
            Other
        ============================*/
        "gcd": {
            category: "numbers",
            syntax: "gcd(number, number)",
            description: "Returns the greatest common divisor of one or more integers.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return gcd_two_numbers(...args);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "lcm": {
            category: "numbers",
            syntax: "lcm(number, number)",
            description: "Returns the least common multiple of one or more integers.",
            arity: 2,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 2: return lcm_two_numbers(...args);
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
        "isprime": {
            category: "numbers",
            syntax: "isprime(number)",
            description: "Returns whether or not a number is a prime number.",
            arity: 1,
            evaluate: function(...args) {
                switch(arguments.length) {
                    case 1: return isPrime(args[0]) ? 1 : 0;
                    default:throw new SyntaxError("Invalid number of arguments")
                }
            },
        },
    }
}