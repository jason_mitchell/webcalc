export function Terminal() {
    var store=Storages.initNamespaceStorage('webcalc').localStorage;
    var term = {}

      /////////////////////
     // Local Variables //
    /////////////////////

    var outputDiv = document.getElementById("Output")
    var inputDiv = document.getElementById("input-line");
    var currentCard;

    var modules = {}
    var hotkeys = {}

    var history = [];
    var historyIndex = 0;
    var historyLimit = 20;
    var ansInserted = false;

    if(store.isSet("history"))
    {
        history = store.get("history")
    }
    /**
     * Loads a module
     * @param {*} path Path to the module, relative to the calling module
     * @param {*} args Any additional arguments to pass to the module
     */
    term.loadModule = async function (path, ...args)
    {
        var mod = await import(path);
        modules[path] = mod;
        console.log("Loaded module:",mod)
        return mod.default(this, ...args);
    }

    term.newCard = function(...classes)
    {
        if(currentCard && currentCard.children().length === 0)
            currentCard.remove();

        var card = $(`<div class="card card-output"></div>`).prependTo(outputDiv)

        classes.forEach(cls => {card.addClass(cls);});

        return currentCard = card;
    }

    term.table = function(type, data, header)
    {
        var table = $(`<table class="table table-sm border"></table>`)
        
        if(header && header.length>0)
        {
            var thead = $(`<thead></thead>`)
            
            for(var i=0; i<header.length; i++)
            {
                thead.append($(`<th scope="col">${header[i]}</th>`))
            }
            table.append(thead)
        }

        var tbody = $(`<tbody></tbody>`)
        for(var row=0; row<data.length; row++)
        {
            var rowData = data[row];
            var tr = $(`<tr></tr>`)

            for(var col=0; col<rowData.length; col++)
            {
                var td = $(`<td>${rowData[col]}</td>`)
                tr.append(td)
            }
            tbody.append(tr)
        }
        table.append(tbody)

        term.line(type, table[0].outerHTML)
    }

    term.line = function(type, data)
    {
        if(!currentCard) term.newCard();

        currentCard.append($(`<div class="Line ${type}">${data}</div>`))
    }

    term.endCard = function()
    {
        currentCard = null;
    }


    term.execute = async function(string)
    {
        term.newCard();
        term.line("input",string);
        
        if(term.onExecute)
        {
            term.onExecute(string)
        }

        history.unshift(string)
        
        while(history.length > historyLimit) {history.pop()}
        
        store.set("history",history)

        term.endCard();
    }

    term.clear = function() { $(".card-output").remove(); }

    inputDiv.oninput = (event) => {
        var parenthesisDepth = 0;
        var bracketDepth = 0;
        
        var inputString = inputDiv.value;
        var stringBuilder = "";

        for(var i = 0; i<inputString.length; i++)
        {
            const char = inputString[i];

            if(char==="(")
            {
                parenthesisDepth++
            }
            else if(char===")")
            {
                if(parenthesisDepth>0)
                {
                    parenthesisDepth--
                }
                else
                { 
                    continue
                }
            }
            else if(char==="{")
            {
                if(bracketDepth==0)
                {
                    bracketDepth++
                }
                else
                {
                    continue
                }
            }
            else if(char==="}")
            {
                if(bracketDepth>0)
                {
                    bracketDepth--
                }
                else
                {
                    continue
                }
            }
            else if(i===0 && ansInserted===false && "!%*/-+".indexOf(char)!==-1)
            {
                stringBuilder = "ans"
                ansInserted = true
            }
            stringBuilder = `${stringBuilder}${char}`
        }
        
        inputDiv.value = stringBuilder
    };

    inputDiv.onkeydown = (event) => {
        const char = event.which || event.keyCode;
        if(hotkeys[char]) { hotkeys[char]() };
    };
    



    /**
     * Registers a new hotkey
     * @param {int} keyCode 
     * @param {function} callback 
     */
    term.hotkey = function(keyCode, callback) {
        hotkeys[keyCode] = callback;
    }
    //Enter hotkey
    term.hotkey(13, function() {
        const string = inputDiv.value;
        if(string === '') {return false}

        historyIndex = 0;
        ansInserted = false;

        term.execute(string);
        inputDiv.value = '';
    });
    
    function loadHistory(index) {
        historyIndex = index;
        inputDiv.value = history[historyIndex-1];
    }

    //Arrow Up hotkey
    term.hotkey(38, function() {
        if(historyIndex < history.length) {
            loadHistory(historyIndex+1);
        }
    });

    //Arrow Down hotkey
    term.hotkey(40, function() {
        if(historyIndex > 0) {
            loadHistory(historyIndex-1);
        }
    });
    
    return term;
}