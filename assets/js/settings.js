
export var Name = "Settings";
export default function(calc) {
    var store=Storages.initNamespaceStorage('webcalc').localStorage;
    /*=====================
        Angle Format
    ======================*/

    $("#btn-degrees").click(() => {calc.angle.setting = "degrees"})
    $("#btn-radians").click(() => {calc.angle.setting = "radians"})

    calc.angle = {
        set setting(string) {
            if(string === this.mode) {return}
            this.mode = string
            
            if(string === "radians") {this.factor = 1}
            if(string === "degrees") {this.factor = Math.PI/180}
            
            $(`#btn-${string}, #btn-${string}2`).attr("checked",true)
            store.set("angleMode",string)
        },
        get setting() {
            if(!store.isSet("angleMode"))
                return "radians";
            return store.get("angleMode")
        }
    }

    /*=====================
        Number Format
    ======================*/
    $("#btn-decimal").click(() => {calc.number.setting = "decimal"})
    $("#btn-scientific").click(() => {calc.number.setting = "scientific"})
    $("#btn-fractional").click(() => {calc.number.setting = "fractional"})

    calc.number = {
        set setting(string) {
            if(string === this.mode) {return}
            this.mode = string

            $(`#btn-${string}, #btn-${string}2`).attr("checked",true)
            store.set("numberMode",string)
        },
        get setting() {
            if(!store.isSet("numberMode"))
                return "decimal";
            return store.get("numberMode")
        }
    }
    
    /*========================
        Developer Setting
    =========================*/
    $("#btn-logging").click(() => {calc.dev.logging = !calc.dev.log})
    $("#btn-debugging").click(() => {calc.dev.debugging = !calc.dev.debug})

    calc.dev = {
        set logging(boolean) {
            if(boolean === this.log) {return}
            this.log = boolean

            $("#btn-logging").attr("checked", boolean)
            store.set("logging", boolean)
        },
        set debugging(boolean) {
            if(boolean === this.debug) {return}
            this.debug = boolean

            console.log(`Debugging ${boolean ? "on" : "off"}`)

            $("#btn-debugging").attr("checked", boolean)
            store.set("debugging", boolean)
        },
        get debugging() {
            if(!store.isSet("debugging"))
                return false;
            return store.get("debugging") || false;
        },
        get logging() {
            if(!store.isSet("logging"))
                return false;
            return store.get("logging") || false;
        }
    }

    //Initialize store
    calc.angle.setting = calc.angle.setting;
    calc.number.setting = calc.number.setting;
    calc.dev.logging = calc.dev.logging;
    calc.dev.debugging = calc.dev.debugging;
}